" Weird fix that allows overriding cursor color
set guicursor=a:Cursor,i:ver25

" On Windows, JetBrains fonts need smaller point size
if has('win64')
    GuiFont! JetBrainsMono\ Nerd\ Font\ Mono:h10
else
    GuiFont! JetBrainsMono\ Nerd\ Font\ Mono:h12
endif

" Don't use the native GUI version
GuiPopupmenu 0
GuiTabline 0
