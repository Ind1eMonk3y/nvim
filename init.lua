local bopt = vim.bo
local cmd  = vim.cmd
local opt  = vim.o
local wopt = vim.wo

--
-- OPTIONS
--

opt.clipboard   = 'unnamedplus' -- Share clipboard with other programs
opt.cmdheight   = 2             -- Reduce the number of 'hit-enter' prompts
opt.hidden      = true          -- Don't warn for unsaved changes in buffers
opt.inccommand  = 'nosplit'     -- Show 'substitute' results while typing
opt.joinspaces  = false         -- Use 1 space after .?! when joining lines
opt.lazyredraw  = true          -- Don't redraw too often
opt.mouse       = 'a'           -- Allow mouse use in every mode
opt.path        = '**'          -- Search files recursively
opt.pumblend    = 13            -- Add some transparency to the popup-menu
opt.showmode    = false         -- Don't show current mode under status line
opt.signcolumn  = 'yes:2'       -- If needed, show signcolumn up to 3 sign max
wopt.number     = true          -- Display line numbers margin

-- Case sensitivity
opt.ignorecase = true -- Ignore case unless '\C' is used (needed by 'smartcase')
opt.smartcase  = true -- Ignore case unless '\C' or upper case is used

-- Command-line completion
opt.wildmenu = true                    -- Use enhanced command-line completion
opt.wildmode = 'longest'               -- Complete until longest common string
opt.wildmode = opt.wildmode .. ',full' -- Complete the next full match

-- Completion
-- opt.completeopt = 'menuone,noinsert,noselect' -- Better completion experience
opt.completeopt = 'menu,menuone,noselect' -- Maybe temporary to try nvim-cmp
opt.shortmess   = opt.shortmess .. 'I'        -- No intro message
opt.shortmess   = opt.shortmess .. 'c'        -- No extra completion messages

-- Conceal
-- wopt.concealcursor = 'n' -- Only conceal cursor line in Normal mode
-- wopt.conceallevel  = 2   -- Hide concealed text unless it has a 'cchar'

-- Cursor
opt.scrolloff   = 3       -- Keep N lines of context above and below the cursor
opt.sidescroll  = 8       -- Use N columns when scrolling horizontally
opt.virtualedit = 'block' -- Cursor can go anywhere in Visual-block mode
wopt.cursorline = true    -- Highlight the whole cursor line

-- Indent
opt.expandtab   = true -- Use spaces instead of tabs
opt.shiftwidth  = 4    -- Use N columns when shifting (>>, <<)
opt.smarttab    = true -- Typing <Tab> in front of a line acts like shifting
opt.softtabstop = 4    -- Use N columns when typing <Tab> or <BS>
opt.tabstop     = 4    -- Use N spaces for 1 <Tab>

-- Modifications/updates
bopt.swapfile  = false -- Let user handle its file writes/backups
opt.backup     = false -- Don't write backup before writing a file
opt.updatetime = 1000  -- Use quicker time for CursorHold

-- Pairs characters matching
opt.matchpairs = opt.matchpairs .. ',<:>' -- Add support for markups
opt.showmatch  = true                     -- Highlight matching pair characters

-- Splits
opt.diffopt = opt.diffopt .. ',vertical' -- Start diff mode with vertical split
opt.equalalways = false                  -- Don't be too smart with splits
opt.splitbelow  = true                   -- Put new windows below
opt.splitright  = true                   -- Put new windows on the right

-- Title bar
opt.title       = true -- Update title bar with the value of 'titlestring'
opt.titlestring = '%F' -- Use the full path of the file open in the buffer

-- Timing
opt.timeout     = true
opt.timeoutlen  = 1000
opt.ttimeout    = true
opt.ttimeoutlen = 5

-- Whitespaces
opt.list      = false -- Don't reveal EOLs, spaces, tabs...
opt.listchars = 'tab:▸ ,space:·,trail:•,nbsp:¤,eol:$'

-- Wrapping
opt.showbreak    = '↳ '  -- Add an indicator at the beginning of wrapped lines
wopt.breakindent = true  -- Visually indent wrapped lines
wopt.wrap        = false -- Use 'wrap' on demand with `[windo ]set wrap`

--
-- Status line
--

vim.g.mode_table = {
  ["i"]  = "INSERT",
  ["n"]  = "NORMAL",  ["no"]    = "NORMAL-O.P.",
  ["!"]  = "SHELL",   ["t"]     = "TERMINAL",
  ["R"]  = "REPLACE", ["Rv"]    = "V-REPLACE",
  ["c"]  = "COMMAND", ["ce"]    = "EX",   ["cv"] = "VIM-EX",
  ["r"]  = "PROMPT",  ["rm"]    = "MORE", ["r?"] = "CONFIRM",
  ["s"]  = "SELECT",  ["S"]     = "S-LINE",
  ["^S"] = "S-BLOCK", ["<C-s>"] = "S-BLOCK",
  ["v"]  = "VISUAL",  ["V"]     = "V-LINE",
  [""] = "V-BLOCK", ["<C-v>"] = "V-BLOCK",
}

opt.statusline = ' %{g:mode_table[mode()]}'
opt.statusline = opt.statusline .. ' | %n:%<%t%m%r%w'
opt.statusline = opt.statusline .. '%= %P L:%l/%L C:%v'
opt.statusline = opt.statusline .. ' | %{&fenc} %{&ff} | %Y '

--
-- LUA
--

P = function(v)
  print(vim.inspect(v))
  return v
end

require('mappings')
require('plugins')
require('my.mappings')

--
-- VIMSCRIPT
--

cmd [[
language en_US.UTF-8

runtime vim/autocommands.vim
runtime vim/commands.vim

if (has("termguicolors"))
    set termguicolors
endif
"colorscheme catppuccin
colorscheme kanagawa

if has("win64")
  let &shell = 'bash'
  let &shellcmdflag = '-c'
endif
]]
