" Don't be too smart with comments
autocmd FileType * set formatoptions=qj

" Override colorscheme with custom hightlightings
autocmd ColorScheme * runtime vim/highlightings.vim

" Highlight everything that's yanked
autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank({timeout = 200})

" By default terminal have no filetype
" (term specific options are set in `~/.config/nvim/after/ftplugin/term.vim`)
autocmd TermOpen term://*  set filetype=term

" Don't use list chars for help file
autocmd FileType help set nolist

" Remove some lines from command line history
autocmd BufReadPost,CmdLineLeave * call histdel(":", '^[qx]\|^bd')
