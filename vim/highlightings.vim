" --- CORE

highlight Character    guifg=#90C0A0
highlight ColorColumn  guibg=#131729
highlight Comment      guifg=#345576
highlight Cursor       guibg=#008492
highlight CursorLine   guibg=#152535
highlight CursorLineNR guifg=#00A0A0 guibg=#132134
highlight DiffAdd      guifg=#40A080
highlight DiffChange   guifg=#B05040 guibg=#081321
highlight DiffDelete   guifg=#E04040
highlight DiffText     guifg=#F09080 guibg=#081321
highlight Folded       guifg=#345589 guibg=#081321
highlight IncSearch    guifg=#55D0EE guibg=#213455 gui=bold
highlight Label        guifg=#DC143C guibg=#132134
highlight LineNR       guifg=#424271 guibg=#132134
highlight NonText      guifg=#213455
highlight MsgArea      guifg=#D0D0D0 guibg=#081321
highlight Normal       guifg=#D0D0D0 guibg=#081321
highlight NormalNC     guifg=#D0D0D0 guibg=#081321
highlight Pmenu        guibg=#132134
highlight Search       guifg=#08B0CE guibg=#213455
highlight SignColumn   guibg=#111929
highlight SpecialKey   guifg=#DC143C
highlight StatusLine   guifg=#6080A0 guibg=#132134
highlight StatusLineNC guifg=#204060 guibg=#132134
highlight String       guifg=#90C0A0
highlight TabLineSel   guifg=#08B0CE guibg=#132134
highlight Todo         guifg=#F09080
highlight VertSplit    guifg=#213455
highlight Visual       guibg=#162945
highlight Whitespace   guifg=#345589

" Highlight trailing spaces
highlight TrailingWhitespaces guibg=#DC143C
match TrailingWhitespaces /\s\+$/

" --- PLUGINS

highlight CmpItemAbbr           guifg=#7676A0
highlight CmpItemAbbrDeprecated guifg=#FFA000
highlight CmpItemAbbrMatch      guifg=#D0D0D0
highlight CmpItemAbbrMatchFuzzy guifg=#A0A0A0
highlight CmpItemKind           guifg=#555589
highlight CmpItemMenu           guifg=#00A000

highlight HopNextKey   guifg=#FF0080
highlight HopNextKey1  guifg=#00C0A0
highlight HopNextKey2  guifg=#00A080
highlight HopUnmatched guifg=#004050

highlight IndentBlanklineChar               guifg=#182640
highlight IndentBlanklineContextChar        guifg=#005576
highlight IndentBlanklineSpaceChar          guifg=#182640
highlight IndentBlanklineSpaceCharBlankline guifg=#182640

highlight DiagnosticError          guifg=#E04040   guibg=#111929
highlight DiagnosticHint           guifg=#40A080   guibg=#111929
highlight DiagnosticInfo           guifg=#4080A0   guibg=#111929
highlight DiagnosticWarn           guifg=#F09080   guibg=#111929
highlight DiagnosticUnderlineError gui=undercurl   guisp=#E04040
highlight DiagnosticUnderlineHint  gui=undercurl   guisp=#40A080
highlight DiagnosticUnderlineInfo  gui=undercurl   guisp=#4080A0
highlight DiagnosticUnderlineWarn  gui=undercurl   guisp=#F09080

highlight TelescopeBorder         guifg=#213455
highlight TelescopePreviewLine    guibg=#134655
highlight TelescopePromptPrefix   guifg=#13C0DE
highlight TelescopeSelection      guifg=#13A0BE
highlight TelescopeSelectionCaret guifg=#13A0BE

highlight rainbowcol1 guifg=#E28C8C guibg=none
highlight rainbowcol2 guifg=#B3E1A3 guibg=none
highlight rainbowcol3 guifg=#EADDA0 guibg=none
highlight rainbowcol4 guifg=#A4B9EF guibg=none
highlight rainbowcol5 guifg=#F0AFE1 guibg=none
highlight rainbowcol6 guifg=#ECBFBD guibg=none
highlight rainbowcol7 guifg=#9DDDCB guibg=none

" marks.nvim
highlight MarkSignHL    guifg=#4080FF guibg=#111929
highlight MarkSignNumHL guifg=none    guibg=none
