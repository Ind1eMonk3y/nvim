setlocal autoindent
setlocal colorcolumn=80
setlocal expandtab
setlocal fileformat=unix
setlocal formatoptions=qj
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal tabstop=2
