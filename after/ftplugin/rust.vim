setlocal autoindent
setlocal colorcolumn=100
setlocal expandtab
setlocal fileformat=unix
setlocal formatoptions=qj
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal tabstop=4
