setlocal autoindent
setlocal colorcolumn=80,88
setlocal expandtab
setlocal fileformat=unix
setlocal formatoptions=qj
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal tabstop=4

" Unless on Windows, use the interpreter set by 'pyenv'
if has('win64')
    let g:python3_host_prog='python'
else
    let g:python3_host_prog=expand('~/.pyenv/shims/python')
endif
