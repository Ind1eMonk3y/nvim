local utils = require('my.utils')
local noremap  = utils.noremap
local cnoremap = utils.cnoremap
local inoremap = utils.inoremap
local nnoremap = utils.nnoremap
local tnoremap = utils.tnoremap

vim.g.mapleader      = ' '
vim.g.maplocalleader = ','

--
-- Buffer / files
--

nnoremap('<leader>b', ':buffer ')   -- Switch between buffers
nnoremap('<leader>e', ':edit ')     -- Edit a file
nnoremap('<leader>w', ':write<CR>') -- Write the current file

-- Edit/source $MYVIMRC
nnoremap('<leader>ve', ':edit $MYVIMRC<CR>')
nnoremap('<leader>vs', ':luafile $MYVIMRC<CR>')

-- Save/restore session (buffers, cursor positions, splits...)
local session_file = vim.fn.expand('~/.cache/nvim_session')
nnoremap('<Leader>ss', ':mksession! ' .. session_file .. '<CR>')
nnoremap('<leader>sr', ':source '     .. session_file .. '<CR>')

--
-- Command-line
--

cnoremap('<C-a>', '<Home>')
cnoremap('<C-e>', '<End>')
cnoremap('<C-b>', '<Left>')
cnoremap('<C-f>', '<Right>')
cnoremap('<M-BS>', '<C-w>')
cnoremap('<M-b>', '<S-Left>')
cnoremap('<M-f>', '<S-Right>')

--
-- Completion
--

-- Use <Tab> and <S-Tab> to navigate through popup menu
inoremap("<Tab>",   "pumvisible() ? '<C-n>' : '<Tab>'",   {expr=true, silent=true})
inoremap("<S-Tab>", "pumvisible() ? '<C-p>' : '<S-Tab>'", {expr=true, silent=true})

--
-- Splits / windows
--

-- Open a new window horizontally or vertically
nnoremap('<leader>h', ':vsplit<CR>')
nnoremap('<leader>v', ':split<CR>')

-- Close current window if it's not the last one
nnoremap('<leader>c', ':wincmd c<CR>')

-- Focus other windows from Insert, Normal and Terminal modes
inoremap('<A-h>', '<C-\\><C-N><C-w>h')
inoremap('<A-j>', '<C-\\><C-N><C-w>j')
inoremap('<A-k>', '<C-\\><C-N><C-w>k')
inoremap('<A-l>', '<C-\\><C-N><C-w>l')
nnoremap('<A-h>', '<C-w>h')
nnoremap('<A-j>', '<C-w>j')
nnoremap('<A-k>', '<C-w>k')
nnoremap('<A-l>', '<C-w>l')
tnoremap('<A-h>', '<C-\\><C-N><C-w>h')
tnoremap('<A-j>', '<C-\\><C-N><C-w>j')
tnoremap('<A-k>', '<C-\\><C-N><C-w>k')
tnoremap('<A-l>', '<C-\\><C-N><C-w>l')

--
-- Tabs
--

nnoremap('<leader>tn', ':tabnew<CR>')
nnoremap('<leader>tc', ':tabclose<CR>')

--
-- Unsorted
--

-- Always goto mark's line & column position
noremap("'",  "`")
noremap("['", "[`")
noremap("]'", "]`")

-- Disable search highlighting
nnoremap('<leader><space>', ':nohlsearch<CR>')

-- Quickfix list
nnoremap('<C-A-k>', ':cprevious<CR>')
nnoremap('<C-A-j>', ':cnext<CR>')

-- Toggle visibility of some UI components
vim.cmd[[
let s:hidden_all = 0
let s:prev_cmdheight  = &cmdheight
let s:prev_laststatus = &laststatus
let s:prev_scrolloff  = &scrolloff
function! ToggleMinimalUI()
    if s:hidden_all  == 0
        let s:hidden_all = 1
        set cmdheight=1
        set laststatus=0
        set nonumber
        set noruler
        set noshowcmd
        set noshowmode
        set scrolloff=0
    else
        let s:hidden_all = 0
        let &cmdheight  = s:prev_cmdheight
        let &laststatus = s:prev_laststatus
        let &scrolloff  = s:prev_scrolloff
        set number
        set ruler
        set showcmd
        set showmode
    endif
endfunction
nnoremap <silent><A-S-h> :silent call ToggleMinimalUI()<CR>
]]
