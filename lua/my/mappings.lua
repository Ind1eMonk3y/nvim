local utils = require('my.utils')
local noremap  = utils.noremap
local inoremap = utils.inoremap
local nnoremap = utils.nnoremap
local snoremap = utils.snoremap
local vnoremap = utils.vnoremap
local xnoremap = utils.xnoremap

--
-- Formatter
--

nnoremap("<localleader>fp",  ":FormatWrite<CR>")

--
-- Telescope
--

nnoremap("<leader>F",  ":lua require('my.telescope').find_files_with_preview()<CR>")
nnoremap("<leader>f",  ":lua require('my.telescope').find_files()<CR>")
nnoremap("<leader>g",  ":lua require('my.telescope').live_grep()<CR>")
nnoremap("<leader>lb", ":lua require('my.telescope').builtin()<CR>")
nnoremap("<leader>lx", ":lua require('my.telescope').file_browser()<CR>")
nnoremap("<leader>r",  ":lua require('my.telescope').buffers()<CR>")
nnoremap("<leader>z",  ":lua require('my.telescope').sessions()<CR>")

--
-- Harpoon
--

nnoremap("<leader>on", ":lua require('harpoon.mark').add_file()<CR>")
nnoremap("<leader>oq", ":lua require('harpoon.ui').nav_file(1)<CR>")
nnoremap("<leader>os", ":lua require('harpoon.ui').nav_file(2)<CR>")
nnoremap("<leader>od", ":lua require('harpoon.ui').nav_file(3)<CR>")
nnoremap("<leader>of", ":lua require('harpoon.ui').nav_file(4)<CR>")
nnoremap("<leader>og", ":lua require('harpoon.ui').nav_file(5)<CR>")
nnoremap("<leader>om", ":lua require('harpoon.ui').toggle_quick_menu()<CR>")
nnoremap("<leader>ot", ":lua require('harpoon.term').gotoTerminal(1)<CR>")
nnoremap("<leader>oc", ":lua require('harpoon.term').sendCommand(1, 'clear')<CR>")
nnoremap("<leader>oa", ":lua require('harpoon.term').sendCommand(1, 1)<CR>")
nnoremap("<leader>oz", ":lua require('harpoon.term').sendCommand(1, 2)<CR>")
nnoremap("<leader>oe", ":lua require('harpoon.term').sendCommand(1, 3)<CR>")
nnoremap("<leader>or", ":lua require('harpoon.term').sendCommand(1, 4)<CR>")
nnoremap("<leader>op", ":lua require('harpoon.cmd-ui').toggle_quick_menu()<CR>")

--
-- VSnip
--

vim.cmd [[
" Expand
imap <expr> <C-j> vsnip#expandable() ? '<Plug>(vsnip-expand)' : '<C-j>'
smap <expr> <C-j> vsnip#expandable() ? '<Plug>(vsnip-expand)' : '<C-j>'

" Expand or jump
imap <expr> <C-l> vsnip#available(1) ? '<Plug>(vsnip-expand-or-jump)' : '<C-l>'
smap <expr> <C-l> vsnip#available(1) ? '<Plug>(vsnip-expand-or-jump)' : '<C-l>'

" Jump forward or backward
imap <expr> <Tab>   vsnip#jumpable(1)  ? '<Plug>(vsnip-jump-next)' : '<Tab>'
smap <expr> <Tab>   vsnip#jumpable(1)  ? '<Plug>(vsnip-jump-next)' : '<Tab>'
imap <expr> <S-Tab> vsnip#jumpable(-1) ? '<Plug>(vsnip-jump-prev)' : '<S-Tab>'
smap <expr> <S-Tab> vsnip#jumpable(-1) ? '<Plug>(vsnip-jump-prev)' : '<S-Tab>'

" Select or cut text to use as $TM_SELECTED_TEXT in the next snippet.
" See https://github.com/hrsh7th/vim-vsnip/pull/50
nmap s <Plug>(vsnip-select-text)
xmap s <Plug>(vsnip-select-text)
nmap S <Plug>(vsnip-cut-text)
xmap S <Plug>(vsnip-cut-text)
]]

--
-- Unsorted
--

-- Hop
noremap('<leader>jl', ":lua require'hop'.hint_lines()<CR>", {silent=true})
noremap('<leader>jc', ":lua require'hop'.hint_char1()<CR>", {silent=true})
noremap('<leader>jw', ":lua require'hop'.hint_words()<CR>", {silent=true})


-- Treesitter hint textobject
vim.cmd [[
omap     m :<C-U>lua require('tsht').nodes()<CR>
vnoremap m :<C-U>lua require('tsht').nodes()<CR>
]]

-- Marks
nnoremap('dm',         "<cmd>lua require('my.marks').delete()<CR>")
nnoremap('dm-',        "<cmd>lua require('my.marks').delete_line()<CR>")
nnoremap('dm<Space>',  "<cmd>lua require('my.marks').delete_buf()<CR>")
nnoremap('<leader>mm', "<cmd>MarksQFListAll<CR>")

