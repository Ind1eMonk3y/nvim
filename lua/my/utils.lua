local M = {}

local noremap = function(mode, lhs, rhs, user_opts)
  local opts = vim.tbl_deep_extend("force", {noremap=true}, user_opts or {})
  vim.api.nvim_set_keymap(mode, lhs, rhs, opts)
end

M.noremap  = function(...) noremap('',  ...) end
M.cnoremap = function(...) noremap('c', ...) end
M.inoremap = function(...) noremap('i', ...) end
M.lnoremap = function(...) noremap('l', ...) end
M.nnoremap = function(...) noremap('n', ...) end
M.onoremap = function(...) noremap('o', ...) end
M.snoremap = function(...) noremap('s', ...) end
M.tnoremap = function(...) noremap('t', ...) end
M.vnoremap = function(...) noremap('v', ...) end
M.xnoremap = function(...) noremap('x', ...) end

return M
