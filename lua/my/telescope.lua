local action_state = require('telescope.actions.state')
local actions      = require('telescope.actions')


local M = {}

local open_selected_files = function(prompt_bufnr)
  local current_picker = action_state.get_current_picker(prompt_bufnr)
  actions.close(prompt_bufnr)
  for _, files in ipairs(current_picker:get_multi_selection()) do
    vim.cmd(":silent edit "..files[1])
  end
end

local find_files_mappings = function(_, map)
  map('i', '<M-cr>', open_selected_files)
  map('n', '<M-cr>', open_selected_files)
  return true
end

M.buffers = function()
  require('telescope.builtin').buffers()
end

M.builtin = function()
  require('telescope.builtin').builtin()
end

M.file_browser = function()
  require('telescope.builtin').file_browser({
    attach_mappings = find_files_mappings
  })
end

M.find_files = function()
  local opts = require('telescope.themes').get_dropdown({
    previewer = false,
  })
  opts.attach_mappings = find_files_mappings
  require('telescope.builtin').find_files(opts)
end

M.find_files_with_preview = function()
  require('telescope.builtin').find_files({
    attach_mappings = find_files_mappings
  })
end

M.live_grep = function()
  require('telescope.builtin').live_grep()
end

M.sessions = function()
  require('telescope').extensions.zessions.sessions{
    layout_config = {
      width = 75,
    }
  }
end

return M
