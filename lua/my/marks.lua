local marks = require('marks')

local M = {}

M.delete = function()
  marks.delete()
  vim.cmd("wshada!")
end

M.delete_line = function()
  marks.delete_line()
  vim.cmd("wshada!")
end

M.delete_buf = function()
  marks.delete_buf()
  vim.cmd("wshada!")
end

return M
