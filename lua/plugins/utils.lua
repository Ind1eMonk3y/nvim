local strfmt = string.format
local M = {}

packages_dir = vim.fn.stdpath('data')..'/site/pack'

local paq_repo_url  = 'https://github.com/savq/paq-nvim'
local paq_opt_dir   = packages_dir..'/paqs/opt'
local paq_start_dir = packages_dir..'/paqs/start'

M.download_paq = function()
  print(strfmt("'Paq' is missing, download it ? [y/N]", prompt))
  local answer = vim.fn.nr2char(vim.fn.getchar()):lower()
  if answer ~= "y" then return end

  local out = vim.fn.system({'git', 'clone', paq_repo_url, paq_opt_dir..'/paq-nvim'})
  print(out)
  print("[INFO] You must restart now !")
end

M.paq_is_installed = function()
  return vim.fn.isdirectory(paq_opt_dir) == 1
end

M.git_submodule_update = function(repo_path)
  local out = vim.fn.system(strfmt(
    'cd %s && git submodule update --init', repo_path
  ))
  print(out)
end

M.update_package_submodule = function(package)
  M.git_submodule_update(paq_start_dir..'/'..package)
end

return M
