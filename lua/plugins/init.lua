local plugins_utils = require('plugins.utils')
local update_package_submodule = plugins_utils.update_package_submodule

--
-- PLUGINS MANAGER
--

-- Check for 'Paq' and download it if needed
if not plugins_utils.paq_is_installed() then return plugins_utils.download_paq() end

-- Load 'Paq' and let it manage itself
vim.cmd 'packadd paq-nvim'
local paq = require('paq').paq
paq {'savq/paq-nvim', opt=true}

--
-- PLUGINS DECLARATION
--

-- Requirements for some plugins
paq 'nvim-lua/plenary.nvim'
paq 'nvim-lua/popup.nvim'


-- Colorschemes
--paq 'catppuccin/nvim'
paq 'rebelot/kanagawa.nvim'

-- Comments
paq 'numToStr/Comment.nvim'
paq 'folke/todo-comments.nvim'

-- Completion
paq 'hrsh7th/nvim-cmp'
paq 'hrsh7th/cmp-buffer'
paq 'hrsh7th/cmp-nvim-lsp'
paq 'hrsh7th/cmp-path'
paq 'hrsh7th/cmp-vsnip'


-- Formatter
paq 'mhartington/formatter.nvim'

-- LSP
paq 'neovim/nvim-lspconfig'

-- Marks
paq 'chentau/marks.nvim'

-- Navigation
paq 'phaazon/hop.nvim'

-- Snippets
paq 'hrsh7th/vim-vsnip'

-- Telescope
paq 'nvim-telescope/telescope.nvim'

-- Telescope based
paq 'Ind1eMonk3y/zessions'
paq 'ThePrimeagen/harpoon'
paq {
  'nvim-telescope/telescope-fzy-native.nvim',
  run = function()
    update_package_submodule('telescope-fzy-native.nvim')
  end,
}

-- Tree-sitter
paq {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}

-- Tree-sitter based
paq 'nvim-treesitter/nvim-treesitter-refactor'
paq 'p00f/nvim-ts-rainbow'
paq 'lukas-reineke/indent-blankline.nvim'
paq 'mfussenegger/nvim-ts-hint-textobject'
paq 'nvim-treesitter/playground'

-- Unsorted
paq 'RRethy/nvim-align'
paq 'kyazdani42/nvim-web-devicons'
paq 'Glench/Vim-Jinja2-Syntax'
paq 'mattn/emmet-vim'
paq 'fatih/vim-go'
paq 'windwp/nvim-autopairs'


--
-- PLUGINS CONFIGURATIONS
--

-- Don't stop loading other files if this 'require' fails
local ok, returned = pcall(require, 'plugins.configs')
if not ok then
  print("[ERROR] Unable to load plugins configurations correctly !")
  if type(returned) == "string" then print(returned:gsub('	', '  ')) end
end
