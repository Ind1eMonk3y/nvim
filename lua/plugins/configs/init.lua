-- Use R to reload module while requiring it
RELOAD = require('plenary.reload').reload_module
R = function(name)
  RELOAD(name)
  return require(name)
end

-- Colorschemes

-- require("catppuccin").setup({
--   styles = {
--     keywords = "NONE",
--   },
--   integrations = {
--     hop        = false,
--     markdown   = true,
--     telescope  = false,
--     treesitter = true,
--     ts_rainbow = true,
--     native_lsp = {
--       enabled = false,
--     },
--   },
-- })

require('kanagawa').setup({
  undercurl = true,           -- enable undercurls
  commentStyle = "italic",
  functionStyle = "NONE",
  keywordStyle = "NONE",
  statementStyle = "NONE",
  typeStyle = "NONE",
  variablebuiltinStyle = "NONE",
  specialReturn = true,       -- special highlight for the return keyword
  specialException = true,    -- special highlight for exception handling keywords
  transparent = false,        -- do not set background color
  colors={
    bg = "#081321",
    fg = "#C0C0C0",
    fujiWhite = "#C0C0C0",
    surimiOrange = "#F09060",

  },
  overrides = {},
})

-- Comments
require("Comment").setup{}

-- Comments tags
require("todo-comments").setup {
-- BUG:  the quick brown fox jumps on the lazy dog
-- WARN: the quick brown fox jumps on the lazy dog
-- TODO: the quick brown fox jumps on the lazy dog
-- NOTE: the quick brown fox jumps on the lazy dog
-- BUG: WARN: TODO: NOTE:
  signs = false,
  keywords = {
    BUG  = { icon="#", color = "error",   alt = { "FIX", "FIXME", "FIXIT", "ISSUE" }, },
    WARN = { icon="#", color = "warning", alt = { "WARNING", "HACK", "XXX" }, },
    TODO = { icon="#", color = "info",    alt = { "DONE", "PENDING" }, },
    NOTE = { icon="#", color = "hint",    alt = { "INFO", "OUTPUT", "TIP" }, },
  },
  highlight = { before = "", keyword = "fg", after = "", },
}

-- Completion
require('plugins.configs.completion')

-- Formatter
require('plugins.configs.formatter')

-- Language Server Protocol
require('plugins.configs.lsp')

-- Marks
require'marks'.setup{
  builtin_marks = { "'", ".", "^", "<", ">" },
  default_mappings = false,
  sign_priority = {
    lower=5,
    upper=10,
    builtin=15,
    bookmark=15
  },
}

-- Navigation
require("hop").setup{keys = 'mlkjoi,qsdfzec'}

-- Snippets - vsnip
vim.g.vsnip_snippet_dir = vim.fn.expand("~/.config/nvim/snips")

-- Telescope
require('plugins.configs.telescope')

-- Tree-Sitter
require('plugins.configs.treesitter')

-- Unsorted
vim.cmd [[command! -range=% -nargs=1 Align lua require'align'.align(<f-args>)]]
require'nvim-web-devicons'.setup {default = true}

vim.g.harpoon_log_level = "error"
require("harpoon").setup({
  global_settings = {
    enter_on_sendcmd = true,
  },
})

-- Emmet
vim.g.user_emmet_leader_key='<C-SPACE>'

-- nvim-autopairs
-- require('nvim-autopairs').setup{
--   disable_filetype          = { "TelescopePrompt" },
--   disable_in_macro          = false,  -- disable when recording or executing a macro
--   disable_in_visualblock    = false, -- disable when insert after visual block mode
--   ignored_next_char         = string.gsub([[ [%w%%%'%[%"%.] ]],"%s+", ""),
--   enable_moveright          = true,
--   enable_afterquote         = true,  -- add bracket pairs after quote
--   enable_check_bracket_line = true,  --- check bracket in same line
--   check_ts                  = true,
--   map_bs                    = true,  -- map the <BS> key
--   map_c_w                   = false, -- map <c-w> to delete a pair if possible
--
--   fast_wrap = {
--     map = '<M-e>',
--     chars = { '{', '[', '(', '"', "'" },
--     pattern = string.gsub([[ [%'%"%)%>%]%)%}%,] ]], '%s+', ''),
--     offset = 0, -- Offset from pattern match
--     end_key = '$',
--     keys = 'qwertyuiopzxcvbnmasdfghjkl',
--     check_comma = true,
--     highlight = 'Search',
--     highlight_grey='Comment',
--   },
-- }
