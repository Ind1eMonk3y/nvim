require('telescope').setup{
  defaults = {
    color_devicons = true,
    prompt_prefix = "❯❯❯ ",
    scroll_strategy = "cycle",
    selection_caret = "❯ ",
    selection_strategy = "reset",
    sorting_strategy = "ascending",
    winblend = 13,

    layout_strategy = "horizontal",
    layout_config = {
      height = 0.93,
      horizontal = {
        width = 0.95,
        preview_width = 0.66,
        prompt_position = "top",
      },
      vertical = {
        width = 0.66,
        preview_height = 0.5,
        mirror = true,
      },
    },
  },

  pickers = {
    buffers = {
      ignore_current_buffer = true,
      only_cwd = false,
      show_all_buffers = true,
      sort_lastused = true,
      sort_mru = true,

      previewer = false,
      theme = "dropdown",
      mappings = {
        i = {["<c-d>"] = "delete_buffer"},
        n = {["<c-d>"] = "delete_buffer"},
      },
    },
    builtin = {
      previewer = false,
      theme = "dropdown",
      layout_config = {
        height=0.93,
      },
    },
    file_browser = {
      hidden = true,
    },
    find_files = {
      find_command = {
        "fd", "--hidden", "--type", "f",
        "--exclude", ".git",
        "--exclude", "node_modules",
      },
    },
    live_grep = {
      -- vimgrep_arguments = {"rg", "--vimgrep"},
      layout_strategy = "vertical",
      layout_config = {
        width = 0.95,
        height = 0.97,
        preview_height = 0.50,
        scroll_speed = 1,
      },
    },
  },

  extensions = {
    fzy_native = {
      override_generic_sorter = true,
      override_file_sorter = true,
    },
  }
}
require('telescope').load_extension('fzy_native')
require('telescope').load_extension('zessions')
