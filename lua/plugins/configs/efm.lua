local lspconfig = require('lspconfig')


local flake8 = {
  lintCommand = "flake8 --max-line-length 160 --format '%(path)s:%(row)d:%(col)d: %(code)s %(code)s %(text)s' --stdin-display-name ${INPUT} -",
  lintFormats = {"%f:%l:%c: %t%n%n%n %m"},
  lintIgnoreExitCode = true,
  lintSource = "flake8",
  lintStdin = true,
}

local mypy = {
  lintCommand = "mypy --show-column-numbers --ignore-missing-imports",
  lintFormats = {
    "%f:%l:%c: %trror: %m",
    "%f:%l:%c: %tarning: %m",
    "%f:%l:%c: %tote: %m",
  },
  lintSource = "mypy",
}

local pylint = {
  lintCommand = "pylint --output-format text --score no --msg-template {path}:{line}:{column}:{C}:{msg} ${INPUT}",
  lintFormats = {"%f:%l:%c:%t:%m"},
  lintIgnoreExitCode = true,
  lintSource = "pylint",
  lintStdin = false,
  lintOffsetColumns = 1,
  lintCategoryMap = {
      I = "H",
      R = "I",
      C = "I",
      W = "W",
      E = "E",
      F = "E",
  },
}

local shellcheck = {
  lintCommand = "shellcheck -f gcc -x -",
  lintFormats = {"%f:%l:%c: %trror: %m", "%f:%l:%c: %tarning: %m", "%f:%l:%c: %tote: %m"},
  lintSource = "shellcheck",
  lintStdin = true,
}

-- npm install -g eslint_d prettier typescript typescript-language-server
local eslint = {
  lintCommand = "eslint_d -f visualstudio --stdin --stdin-filename ${INPUT}",
  lintIgnoreExitCode = true,
  lintStdin = true,
  lintFormats = {
    "%f(%l,%c): %tarning %m",
    "%f(%l,%c): %rror %m"
  },
  lintSource = "eslint"
}

lspconfig.efm.setup{
  init_options = {
    codeAction         = true,
    completion         = true,
    documentFormatting = true,
    documentSymbol     = true,
    hover              = true,
  },
  root_dir = vim.loop.cwd,
  filetypes = {
    "python", "sh",
    "javascript", "javascriptreact", "typescript", "typescriptreact",
  },
  settings = {
      rootMarkers = {".git/"},
      languages = {
          python = {flake8},
          sh     = {shellcheck},

          javascript      = {eslint},
          javascriptreact = {eslint},
          typescript      = {eslint},
          typescriptreact = {eslint},
      }
  },
}
