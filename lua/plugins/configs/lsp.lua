local lspconfig = require('lspconfig')

--
-- LSP Diagnostic Signs
--

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = false,
    signs = true,
    update_in_insert = true,
  }
)
--
-- LSP - on_attach
--

local on_attach = function(client, bufnr)

  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  local b_nnoremap = function(lhs, rhs)
    vim.api.nvim_buf_set_keymap(bufnr, 'n', lhs, rhs, {noremap=true})
  end

  b_nnoremap('<localleader>gd', '<Cmd>lua vim.lsp.buf.definition()<CR>')
  b_nnoremap('<localleader>K',  '<Cmd>lua vim.lsp.buf.hover()<CR>')
  b_nnoremap('<C-k>',           '<cmd>lua vim.lsp.buf.signature_help()<CR>')
  b_nnoremap('<localleader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>')
  b_nnoremap('<localleader>gr', '<cmd>lua vim.lsp.buf.references()<CR>')

  b_nnoremap('<localleader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>')
  b_nnoremap('<localleader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>')
  b_nnoremap('<localleader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>')

  b_nnoremap('<localleader>e',  '<cmd>lua vim.diagnostic.open_float()<CR>')
  b_nnoremap('[d',              '<cmd>lua vim.diagnostic.goto_prev()<CR>')
  b_nnoremap(']d',              '<cmd>lua vim.diagnostic.goto_next()<CR>')
  b_nnoremap('<localleader>q',  '<cmd>lua vim.diagnostic.setqflist()<CR>')

  -- Set some keymaps conditional on server capabilities
  b_nnoremap('<localleader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>')
  b_nnoremap('<localleader>g', '<cmd>lua vim.lsp.buf.range_formatting()<CR>')

  -- not always handled
  b_nnoremap('<localleader>gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>')
  b_nnoremap('<localleader>gi', '<cmd>lua vim.lsp.buf.implementation()<CR>')
  b_nnoremap('<localleader>D',  '<cmd>lua vim.lsp.buf.type_definition()<CR>')

  -- Set autocommands conditional on server_capabilities
  if client.resolved_capabilities.document_highlight then
    vim.cmd [[
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold  <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]]
  end
end


-- ------------------------------------------------------------------------
-- SERVER SPECIFIC
-- ------------------------------------------------------------------------

local default_config = {
  on_attach = on_attach,
  root_dir = vim.loop.cwd,
}

lspconfig.bashls.setup(default_config)
lspconfig.gopls.setup(default_config)
lspconfig.jedi_language_server.setup(default_config)
lspconfig.volar.setup(default_config)

--
-- EFM Language Server
--
require('plugins.configs.efm')

--
-- Javascript
--
-- npm install -g eslint_d prettier typescript typescript-language-server
lspconfig.tsserver.setup {
  on_attach = function(client)
    -- Avoid conflict with efm formatting configuration
    client.resolved_capabilities.document_formatting = false
    on_attach(client)
  end,
  root_dir = vim.loop.cwd,
}

----
---- Rust
----
--lspconfig.rust_analyzer.setup {
--  on_attach=on_attach,
--  root_dir = vim.loop.cwd,
--  settings = {
--    ["rust-analyzer"] = {
--      assist = {
--        importGranularity = "module",
--        importPrefix = "by_self",
--      },
--      cargo = {
--        loadOutDirsFromCheck = true
--      },
--      procMacro = {
--        enable = true
--      },
--    }
--  }
--}

-----
----- Perl
-----
-- Used only for completion
lspconfig.perlpls.setup{
  on_attach = on_attach,
  root_dir = vim.loop.cwd,
  settings = {
    perl = {
      perlcritic = { enabled = false },
      perltidy   = { enabled = false },
    },
  }
}
-- Used for the rest
lspconfig.perlls.setup{
  on_attach = on_attach,
  root_dir = vim.loop.cwd,
  cmd = {
    "perl", "-MPerl::LanguageServer",
    "-e", "Perl::LanguageServer::run", "--", "--port 13603", "--nostdio 0", "--version 2.3.0"
  },
  filetypes = { "perl" },
  settings  = {
    perl = {
      fileFilter = { ".pm", ".pl" },
      ignoreDirs = ".git",
      perlCmd    = "perl",
      perlInc    = " ",
    },
  },
  single_file_support = true,
}
