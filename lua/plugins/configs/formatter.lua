local prettier = function()
  return {
    exe = "prettier",
    args = {"--stdin-filepath", vim.api.nvim_buf_get_name(0)},
    stdin = true
  }
end

local black = function()
  return {
    exe = "black",
    args = {"-"},
    stdin = true,
  }
end
local isort = function()
  return {
    exe = "isort",
    args = {"--stdout", "--force-single-line", "--profile", "black", "-"},
    stdin = true,
  }
end

-- local rustfmt = function()
--   return {
--     exe = "rustfmt",
--     args = {"--emit=stdout"},
--     stdin = true
--   }
-- end

local shfmt = function()
  return {
    exe = "shfmt",
    args = {
      "-s",       -- simplify the code
			"-ln=bash", -- use bash as language variant
			"-i=2",     -- use 2 spaces for indent
			"-bn",      -- put && and | on a new line
			"-ci",      -- indent switch cases
			"-sr",      -- put a space after redirect operators
			"-"         -- read content from stdin
    },
    stdin = true
  }
end

local perltidy = function()
  return {
    exe = "perltidy",
    args = {"-st"},
    stdin = true,
  }
end

require("formatter").setup({
  logging = true,
  filetype = {
    css             = {prettier},
    html            = {prettier},
    javascript      = {prettier},
    javascriptreact = {prettier},
    json            = {prettier},
    typescript      = {prettier},
    typescriptreact = {prettier},
    vue             = {prettier},
    yaml            = {prettier},

    python = {black, isort},
    -- rust   = {rustfmt},
    sh     = {shfmt},
    perl   = {perltidy},
  }
})

