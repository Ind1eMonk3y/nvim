--
-- Indent guides
--

vim.g.indent_blankline_char                 = '│'
vim.g.indent_blankline_show_current_context = true
vim.g.indent_blankline_use_treesitter       = true

vim.g.indent_blankline_buftype_exclude  = {'help', 'prompt', 'terminal'}
vim.g.indent_blankline_filetype_exclude = {'help', 'man', 'prompt'}
vim.g.indent_blankline_context_patterns = {
  '^for', '^if', '^object', '^table', '^while',
  'arguments', 'block', 'catch_clause', 'class', 'else_clause',
  'function', 'if_statement', 'import_statement', 'method',
  'operation_type', 'return', 'try_statement',
-- , 'jsx_element', 'jsx_self_closing_element',
}


--
-- Tree-sitter
--

require'nvim-treesitter.configs'.setup{
  ensure_installed = {
    'bash', 'css', 'devicetree', 'go', 'graphql', 'html',
    'javascript' , 'jsdoc', 'json', 'jsonc', 'lua', 'php', 'python', 'perl',
    'query', 'regex', 'rust' , 'toml', 'tsx', 'typescript', 'vim', 'vue', 'yaml'
  },
  highlight = {enable = true},
  rainbow   = {enable = true},
  incremental_selection = {
    enable  = true,
    keymaps = {
      init_selection    = "<a-space>",
      node_decremental  = "<a-p>",
      node_incremental  = "<a-n>",
      scope_incremental = "<a-s>",
    },
  },
  refactor = {
    smart_rename = {
      enable  = true,
      keymaps = {
        smart_rename = "<a-r>",
      },
    },
    navigation = {
      enable  = true,
      keymaps = {
        goto_definition      = "gnd",
        goto_next_usage      = "<a-*>",
        goto_previous_usage  = "<a-#>",
        list_definitions     = "gnD",
        list_definitions_toc = "gO",
      },
    },
  },
  playground = {
    enable = true,
    disable = {},
    updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
    persist_queries = false, -- Whether the query persists across vim sessions
    keybindings = {
      toggle_query_editor = 'o',
      toggle_hl_groups = 'i',
      toggle_injected_languages = 't',
      toggle_anonymous_nodes = 'a',
      toggle_language_display = 'I',
      focus_language = 'f',
      unfocus_language = 'F',
      update = 'R',
      goto_node = '<cr>',
      show_help = '?',
    },
  },
}

