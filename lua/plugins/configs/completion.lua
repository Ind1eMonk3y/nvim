local cmp = require'cmp'
local lspconfig = require('lspconfig')
local cmp_nvim_lsp = require('cmp_nvim_lsp')

cmp.setup({
  mapping = {
    ['<Tab>']     = cmp.mapping.select_next_item(),
    ['<S-Tab>']   = cmp.mapping.select_prev_item(),
    ['<C-u>']     = cmp.mapping.scroll_docs(-4),
    ['<C-d>']     = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>']     = cmp.mapping.close(),
    ['<CR>']      = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Insert,
      select = true,
    })

  },
  snippet = {
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  sorting = {
    comparators = {
      cmp.config.compare.score,
    }
  },
  sources = {
    { name = 'nvim_lsp'     , priority = 8 },
    { name = 'nvim_lua'     , priority = 8 },
    { name = 'vsnip'        , priority = 5 },
    { name = 'path'         , priority = 3 },
    { name = 'buffer'       , priority = 3 },
  },
})

-- Setup nvim-cmp for LSP servers
for _, server in pairs(lspconfig.available_servers()) do
  if not (server == "efm") then
    lspconfig[server].setup {
      capabilities = cmp_nvim_lsp.update_capabilities(
        vim.lsp.protocol.make_client_capabilities()
      )
    }
  end
end
